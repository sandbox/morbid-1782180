
CONTENTS OF THIS FILE
---------------------

 * Summary
 * Dependencies
 * Installation
 * Configuration
 * Customisation


SUMMARY
-------

Allows fields attached to nodes to be updated via comments. Differences are
tracked by node revisions.


DEPENDENCIES
------------

1. Field (core module)

2. Comment (core module)


INSTALLATION
------------

1. Copy this comment_update/ directory to your sites/all/modules directory.

2. Enable the module.


CONFIGURATION
-------------

1. If you haven't already done so, enable comments on content types where you
   wish the fields to be updatable.

2. Edit the settings for the fields you wish to be updatable and check the
   "Allow updating of this field from comments" checkbox.

3. The updatable fields should now appear on the comment form.

4. The "Administer Comment Update" permission may be granted to allow users to
   alter the following Comment Update settings:

   a. The legend used for the "Updatable fields" fieldset on the comment form
      may be configured per content type by clicking the "edit" link on the
      appropriate field settings page. For the "Basic page" content type this
      would be at admin/structure/types/manage/page/comment/fields.

   b. The log message written to the revision log when Comment Update saves a
      new node revision may be configured at admin/config/system/comment_update.
      This setting supports tokens derived from the comment object, for example,
      [comment:cid] and [comment:title]. If the Token module is enabled, a table
      of available tokens will be displayed below the input textfield.

Note: Only users with update permission on the node (as determined by Drupal's
node_access() function) will be able to see the updatable fields on the comment
form and submit updates.


CUSTOMISATION
-------------

To override the default table display for field updates, you may:

  1. Override the table CSS:

     .comment-update-table { ... }

     (See comment_update.css for full CSS).

  2. Alter the output by overriding the theme function:

     Copy the entire theme_comment_updates() function into your template.php,
     rename it to phptemplate_comment_updates() or THEMENAME_comment_updates(),
     and customize the output according to your needs.

     Remember that the output is cached. To see changes from your theme override
     function, you must clear your site cache (via the "Clear all caches" button
     at admin/config/development/performance).
